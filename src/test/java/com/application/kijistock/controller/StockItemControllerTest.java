package com.application.kijistock.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.application.kijistock.constant.KijiStockResponseResult;
import com.application.kijistock.dao.StockItemDao;
import com.application.kijistock.entity.StockItemEntity;
import com.application.kijistock.exception.handler.KijiStockExceptionHandler;
import com.application.kijistock.logic.StockItemLogic;
import com.application.kijistock.request.ItemStockRequest;
import com.application.kijistock.response.KijiStockResponse;
import com.application.kijistock.service.StockItemService;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.Data;

@RunWith(Enclosed.class)
public class StockItemControllerTest {

    /**
    * GetStockItemList Validation Test.
    */
    @RunWith(SpringRunner.class)
    @WebMvcTest(StockItemController.class)
    public static class ValidationTest {

        @Autowired
        private MockMvc mockMvc;

        /**
        * Validation Check Error (invalid format).
        *
        * @throws Exception
        */
        @Test
        public void Validation_check_error_pattern_invalid_format() throws Exception {
            mockMvc.perform(get("/item/list")
                    .header("userId", " ")
                    .param("folderId", " ")
                    .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().is4xxClientError())
                    .andExpect(jsonPath("resultCode").value(KijiStockResponseResult.CE002_VALIDATION_ERROR))
                    .andExpect(jsonPath("resultMessage").value("Request is invalid..."));
        }

        @SpringBootApplication(scanBasePackageClasses = { StockItemController.class, KijiStockExceptionHandler.class })
        static class SpringConfig {
            @Bean
            public StockItemDao stockItemDao() throws Exception {
                return Mockito.mock(StockItemDao.class);
            }

            @Bean
            public StockItemLogic kijiStockLogic() throws Exception {
                return Mockito.mock(StockItemLogic.class);
            }

            @Bean
            public StockItemService kijiStockService() throws Exception {
                return Mockito.mock(StockItemService.class);
            }
        }
    }

    /**
     * StockItem Validation Test.
     */
    @RunWith(Theories.class)
    public static class StockItemValidationTest {

        @Rule
        public MockitoRule mockito = MockitoJUnit.rule();
        private MockMvc mockMvc;
        private ObjectMapper objectMapper = new ObjectMapper();

        @InjectMocks
        private StockItemController sut;
        @Mock
        private StockItemService service;

        private static final String USER_ID = "cognitoId";
        private static final String FOLDER_ID = "folderId";
        private static final String URL = "https://www.google.co.jp/";

        @AllArgsConstructor
        @Data
        public static class ValidationCheckObject {
            private String errorMessage;
            private ItemStockRequest request;
        }

        @Before
        public void setup() {
            mockMvc = MockMvcBuilders.standaloneSetup(sut).setControllerAdvice(new KijiStockExceptionHandler()).build();
        }

        /**
        * ValidationCheck (Success Pattern)
        */
        @DataPoints({ "successPattern" })
        public static ItemStockRequest[] successPattern = {
                new ItemStockRequest(USER_ID, FOLDER_ID, URL),
        };

        /**
        * ValidationCheck (Error Pattern)
        */
        @DataPoints({ "errorPattern" })
        public static ValidationCheckObject[] errorPattern = {
                new ValidationCheckObject("user id must not be null", new ItemStockRequest(null, FOLDER_ID, URL)),
                new ValidationCheckObject("user id must not be empty", new ItemStockRequest("", FOLDER_ID, URL)),
                new ValidationCheckObject("folderId must not be null", new ItemStockRequest(USER_ID, null, URL)),
                new ValidationCheckObject("folderId must not be empty", new ItemStockRequest(USER_ID, " ", URL)),
                new ValidationCheckObject("url must not be null", new ItemStockRequest(USER_ID, FOLDER_ID, null)),
                new ValidationCheckObject("url must not be empty", new ItemStockRequest(USER_ID, FOLDER_ID, " ")),
        };

        /**
         * Validation Check Success.
         *
         * @throws Exception
         */
        @Theory
        public void Validation_check_success_pattern(@FromDataPoints("successPattern") ItemStockRequest request) throws Exception {
            doNothing().when(service).stockItem(request);
            final String json = objectMapper.writeValueAsString(request);
            mockMvc.perform(post("/item/stock")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("resultCode").value(KijiStockResponseResult.N000));
        }

        /**
        * Validation Check Error.
        *
        * @throws Exception
        */
        @Theory
        public void Validation_check_error_pattern(@FromDataPoints("errorPattern") ValidationCheckObject validationCheckObject) throws Exception {
            final String json = objectMapper.writeValueAsString(validationCheckObject.getRequest());
            mockMvc.perform(post("/item/stock")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json))
                    .andExpect(status().is4xxClientError())
                    .andExpect(jsonPath("resultCode").value(KijiStockResponseResult.CE002_VALIDATION_ERROR))
                    .andExpect(jsonPath("resultMessage").value("Request is invalid..."));
        }
    }

    /**
    * GetStockItemList Test.
    */
    @RunWith(MockitoJUnitRunner.class)
    public static class GetStockItemListTest {

        private MockMvc mockMvc;
        private ObjectMapper objectMapper = new ObjectMapper();

        @InjectMocks
        private StockItemController sut;
        @Mock
        private StockItemService service;

        private StockItemEntity entity;

        private static final int STOCK_ITEM_ID = NumberUtils.INTEGER_ONE;
        private static final String USER_ID = "cognitoId";
        private static final String FOLDER_ID = "folderId";
        private static final String URL = "https://www.google.co.jp/";
        private static final String TITLE = "Google";
        private static final String CONTENT = "Google content";
        private static final String FAVICON_PATH = "Google favicon path";

        @Before
        public void setup() throws Exception {
            mockMvc = MockMvcBuilders.standaloneSetup(sut).setControllerAdvice(new KijiStockExceptionHandler()).build();
            entity = StockItemEntity.builder()
                    .stockItemId(STOCK_ITEM_ID)
                    .userId(USER_ID)
                    .folderId(FOLDER_ID)
                    .url(URL)
                    .title(TITLE)
                    .content(CONTENT)
                    .faviconPath(FAVICON_PATH)
                    .build();
        }

        @Test
        public void success() throws Exception {
            when(service.getStockItemList(USER_ID, FOLDER_ID)).thenReturn(Arrays.asList(entity));
            final KijiStockResponse<List<StockItemEntity>> response = new KijiStockResponse<>();
            response.setResultCode(KijiStockResponseResult.N000);
            response.setData(Arrays.asList(entity));
            final MvcResult mvcResult = mockMvc.perform(get("/item/list")
                    .header("userId", USER_ID)
                    .param("folderId", FOLDER_ID)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            final String actualResult = mvcResult.getResponse().getContentAsString();
            final String expectedResult = objectMapper.writeValueAsString(response);
            assertThat(actualResult, is(expectedResult));
        }
    }

    /**
    * StockItem Test.
    */
    @RunWith(MockitoJUnitRunner.class)
    public static class StockItemTest {

        private MockMvc mockMvc;
        private ObjectMapper objectMapper = new ObjectMapper();

        @InjectMocks
        private StockItemController sut;
        @Mock
        private StockItemService service;

        private ItemStockRequest request;

        private String json = null;

        private static final String USER_ID = "cognitoId";
        private static final String FOLDER_ID = "folderId";
        private static final String URL = "https://www.google.co.jp/";

        @Before
        public void setup() throws Exception {
            mockMvc = MockMvcBuilders.standaloneSetup(sut).setControllerAdvice(new KijiStockExceptionHandler()).build();
            request = new ItemStockRequest();
            request.setUserId(USER_ID);
            request.setFolderId(FOLDER_ID);
            request.setUrl(URL);
            json = objectMapper.writeValueAsString(request);
        }

        @Test
        public void success() throws Exception {
            doNothing().when(service).stockItem(request);
            final KijiStockResponse<?> response = new KijiStockResponse<>();
            response.setResultCode(KijiStockResponseResult.N000);
            final MvcResult mvcResult = mockMvc.perform(post("/item/stock")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json))
                    .andExpect(status().isOk())
                    .andReturn();
            final String actualResult = mvcResult.getResponse().getContentAsString();
            final String expectedResult = objectMapper.writeValueAsString(response);
            assertThat(actualResult, is(expectedResult));
        }
    }

    /**
     * DeleteStockItem Test.
     */
    @RunWith(MockitoJUnitRunner.class)
    public static class DeleteStockItemTest {

        private MockMvc mockMvc;
        private ObjectMapper objectMapper = new ObjectMapper();

        @InjectMocks
        private StockItemController sut;
        @Mock
        private StockItemService service;

        private static final int STOCK_ITEM_ID = NumberUtils.INTEGER_ONE;

        @Before
        public void setup() throws Exception {
            mockMvc = MockMvcBuilders.standaloneSetup(sut).setControllerAdvice(new KijiStockExceptionHandler()).build();
        }

        @Test
        public void success() throws Exception {
            doNothing().when(service).deleteStockItem(STOCK_ITEM_ID);
            final KijiStockResponse<?> response = new KijiStockResponse<>();
            response.setResultCode(KijiStockResponseResult.N000);
            final MvcResult mvcResult = mockMvc.perform(delete("/item/delete")
                    .param("stockItemId", String.valueOf(STOCK_ITEM_ID))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
            final String actualResult = mvcResult.getResponse().getContentAsString();
            final String expectedResult = objectMapper.writeValueAsString(response);
            assertThat(actualResult, is(expectedResult));
        }
    }
}
