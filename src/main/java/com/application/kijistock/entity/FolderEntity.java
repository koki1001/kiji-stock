package com.application.kijistock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author koki.esaki
 */
@Entity
@Table(name = "folder")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FolderEntity {

    /** Folder Id Property. */
    @Id
    @Column(name="folder_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "folder_id_seq")
    @SequenceGenerator(name = "folder_id_seq", sequenceName = "folder_id_seq", allocationSize = 1)
    private Integer folderId;

    /** User Id Property. */
    @Column(name="user_id", nullable = false)
    private String userId;

    /** Folder Name Property. */
    @Column(name="name", nullable = false)
    private String name;
}