package com.application.kijistock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author koki.esaki
 */
@Entity
@Table(name = "stock_item")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StockItemEntity {

    /** Stock Item Id Property. */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stock_item_id_seq")
    @SequenceGenerator(name = "stock_item_id_seq", sequenceName = "stock_item_id_seq", allocationSize = 1)
    private Integer stockItemId;

    /** User Id Property. */
    @Column(nullable = false)
    private String userId;

    /** Folder Id Property. */
    @Column(nullable = false)
    private String folderId;

    /** URL Property. */
    @Column(nullable = false)
    private String url;

    /** URL Title Property. */
    @Column(nullable = false)
    private String title;

    /** URL Content Property. */
    @Column(nullable = false)
    private String content;

    /** URL Favicon Path Property. */
    @Column(nullable = false)
    private String faviconPath;
}