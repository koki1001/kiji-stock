package com.application.kijistock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author koki.esaki
 *
 * Main.
 */
@SpringBootApplication
public class KijiStockApplication {
    public static void main(String[] args) {
        SpringApplication.run(KijiStockApplication.class, args);
    }
}