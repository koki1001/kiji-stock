package com.application.kijistock.controller;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.application.kijistock.constant.KijiStockResponseResult;
import com.application.kijistock.entity.StockItemEntity;
import com.application.kijistock.request.ItemStockRequest;
import com.application.kijistock.response.KijiStockResponse;
import com.application.kijistock.service.StockItemService;

/**
 * @author koki.esaki
 */
@Validated
@CrossOrigin
@RestController
@RequestMapping(value = "/item", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
public class StockItemController {

    @Autowired
    StockItemService stockItemService;

    @GetMapping(value = "/list")
    @ResponseStatus(org.springframework.http.HttpStatus.OK)
    @ResponseBody
    public KijiStockResponse<List<StockItemEntity>> getStockItemList(@NotBlank @RequestHeader final String userId, @RequestParam final String folderId) {
        final KijiStockResponse<List<StockItemEntity>> response = new KijiStockResponse<>();
        response.setResultCode(KijiStockResponseResult.N000);
        response.setData(stockItemService.getStockItemList(userId, folderId));
        return response;
    }

    @PostMapping(value = "/stock")
    @ResponseStatus(org.springframework.http.HttpStatus.OK)
    @ResponseBody
    public KijiStockResponse<?> stockItem(@Validated @RequestBody final ItemStockRequest itemStockRequest) {
        final KijiStockResponse<?> response = new KijiStockResponse<>();
        response.setResultCode(KijiStockResponseResult.N000);
        stockItemService.stockItem(itemStockRequest);
        return response;
    }

    @DeleteMapping(value = "/delete")
    @ResponseStatus(org.springframework.http.HttpStatus.OK)
    @ResponseBody
    public KijiStockResponse<?> deleteStockItem(@NotNull @RequestParam final Integer stockItemId) {
        final KijiStockResponse<?> response = new KijiStockResponse<>();
        response.setResultCode(KijiStockResponseResult.N000);
        stockItemService.deleteStockItem(stockItemId);
        return response;
    }
}