package com.application.kijistock.controller;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.application.kijistock.constant.KijiStockResponseResult;
import com.application.kijistock.entity.FolderEntity;
import com.application.kijistock.request.FolderSaveRequest;
import com.application.kijistock.response.KijiStockResponse;
import com.application.kijistock.service.FolderService;

/**
 * @author koki.esaki
 */
@Validated
@CrossOrigin
@RestController
@RequestMapping(value = "/folder", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
public class FolderController {

    @Autowired
    FolderService folderService;

    @GetMapping(value = "/list")
    @ResponseStatus(org.springframework.http.HttpStatus.OK)
    @ResponseBody
    public KijiStockResponse<List<FolderEntity>> getFolderList(@NotBlank @RequestHeader("userId") final String userId) {
        final KijiStockResponse<List<FolderEntity>> response = new KijiStockResponse<>();
        response.setResultCode(KijiStockResponseResult.N000);
        response.setData(folderService.getFolderList(userId));
        return response;
    }

    @PostMapping(value = "/add")
    @ResponseStatus(org.springframework.http.HttpStatus.OK)
    @ResponseBody
    public KijiStockResponse<?> saveFolder(@Validated @RequestBody final FolderSaveRequest folderSaveRequest) {
        final KijiStockResponse<?> response = new KijiStockResponse<>();
        response.setResultCode(KijiStockResponseResult.N000);
        folderService.saveFolder(folderSaveRequest);
        return response;
    }

    @DeleteMapping(value = "/delete")
    @ResponseStatus(org.springframework.http.HttpStatus.OK)
    @ResponseBody
    public KijiStockResponse<?> deleteFolder(@NotNull @RequestParam("folderId") final Integer folderId) {
        final KijiStockResponse<?> response = new KijiStockResponse<>();
        response.setResultCode(KijiStockResponseResult.N000);
        folderService.deleteFolder(folderId);
        return response;
    }
}