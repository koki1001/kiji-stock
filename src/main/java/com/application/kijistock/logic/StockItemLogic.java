package com.application.kijistock.logic;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.application.kijistock.dao.StockItemDao;
import com.application.kijistock.dto.UrlInfo;
import com.application.kijistock.entity.StockItemEntity;
import com.application.kijistock.request.ItemStockRequest;

import lombok.NonNull;

/**
 * @author koki.esaki
 */
@Component
public class StockItemLogic {

    @Autowired
    StockItemDao stockItemDao;

    private static final String GOOGLE_FAVICON_PATH = "http://www.google.com/s2/favicons?domain=";
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36";

    /**
     * Find stock item list by given userId and folderId.
     *
     * @param userId
     * @param folderId
     * @return list of StockItemEntity
     */
    public List<StockItemEntity> findByUserIdAndFolderId(@NonNull final String userId, @NonNull final String folderId) {
        return stockItemDao.findByUserIdAndFolderId(userId, folderId);
    }

    /**
     * Save item by given userId and url.
     *
     * @param itemStockRequest
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws URISyntaxException
     */
    public void save(@NonNull final ItemStockRequest itemStockRequest) throws IOException, IllegalArgumentException, URISyntaxException {
        final UrlInfo urlInfo = createUrlInformation(itemStockRequest.getUrl());
        final String urlTitle = urlInfo.getTitle();
        final String urlContent = urlInfo.getContent();
        final String faviconPath = GOOGLE_FAVICON_PATH + urlInfo.getDomain();
        stockItemDao.save(StockItemEntity.builder()
                .userId(itemStockRequest.getUserId())
                .folderId(itemStockRequest.getFolderId())
                .url(itemStockRequest.getUrl())
                .title(urlTitle)
                .content(urlContent)
                .faviconPath(faviconPath)
                .build());
    }

    /**
     * Create url information.
     *
     * @param url
     * @return UrlInfo
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws URISyntaxException
     */
    private UrlInfo createUrlInformation(final String url) throws IOException, IllegalArgumentException, URISyntaxException {
        final Document document = Jsoup.connect(url).userAgent(USER_AGENT).get();
        final Elements elements = document.body().getAllElements();
        final StringBuilder builder = new StringBuilder();
        for (final Element element : elements) {
            if (element.ownText() == null) {
                continue;
            }
            builder.append(element.ownText()).append("\n");
        }
        return UrlInfo.builder()
                .title(document.title())
                .content(builder.toString())
                .domain(createUrlDomain(url))
                .build();
    }

    /**
     * Create url domain.
     *
     * @param url
     * @return url domain
     * @throws URISyntaxException
     */
    private String createUrlDomain(final String url) throws URISyntaxException {
        final URI uri = new URI(url);
        final String domain = uri.getHost();
        return domain != null ? domain : "";
    }

    /**
     * Delete stock item by given stockItemId.
     *
     * @param stockItemId
     */
    public void deleteByStockItemId(final Integer stockItemId) {
        stockItemDao.deleteByStockItemId(stockItemId);
    }
}
