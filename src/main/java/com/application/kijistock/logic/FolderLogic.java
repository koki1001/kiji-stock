package com.application.kijistock.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.application.kijistock.dao.FolderDao;
import com.application.kijistock.entity.FolderEntity;

import lombok.NonNull;

/**
 * @author koki.esaki
 */
@Component
public class FolderLogic {

    @Autowired
    FolderDao folderDao;

    /**
     * Find folder list by given userId.
     *
     * @param userId
     * @return list of FolderEntity
     */
    public List<FolderEntity> findByUserId(@NonNull final String userId) {
        return folderDao.findByUserId(userId);
    }

    /**
     * Save folder by given userId and folderName.
     *
     * @param userId
     * @param folderName
     */
    public void saveByUserIdAndFolderName(@NonNull final String userId, @NonNull final String folderName) {
        folderDao.save(FolderEntity.builder()
                .userId(userId)
                .name(folderName)
                .build());
    }

    /**
     * Delete folder by given folderId.
     *
     * @param folderId
     */
    public void deleteByFolderId(final Integer folderId) {
        folderDao.deleteByFolderId(folderId);
    }
}
