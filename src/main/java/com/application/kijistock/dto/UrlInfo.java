package com.application.kijistock.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author koki.esaki
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UrlInfo {

    /** URL Title Property. */
    private String title;

    /** URL Content Property. */
    private String content;

    /** URL Domain Property. */
    private String domain;
}
