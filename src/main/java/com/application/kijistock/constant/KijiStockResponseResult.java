package com.application.kijistock.constant;

/**
 * @author koki.esaki
 */
public final class KijiStockResponseResult {

    /** Response Normal Result. */
    public static final String N000 = "N_000";

    /** Response Error CE_001 (Data Not found). */
    public static final String CE001_DATA_NOT_FOUND = "CE_001";

    /** Response Error CE_002 (Invalid Parameter). */
    public static final String CE002_VALIDATION_ERROR = "CE_002";

    /** Response Error CE_003 (API Connection Error). */
    public static final String CE003_API_CONNECTION_ERROR = "CE_003";

    /** Response Error CE_999 (An unexpected error occurred). */
    public static final String CE999_UNEXPECTED_ERROR = "CE_999";
}
