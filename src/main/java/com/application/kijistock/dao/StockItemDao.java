package com.application.kijistock.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.application.kijistock.entity.StockItemEntity;

import lombok.NonNull;

/**
 * @author koki.esaki
 */
@Repository
public interface StockItemDao extends JpaRepository<StockItemEntity, String> {

    /**
     * Find stock item list by given userId.
     *
     * @param userId
     * @param folderId
     * @return list of StockItemEntity
     */
    public List<StockItemEntity> findByUserIdAndFolderId(@NonNull final String userId, @NonNull final String folderId);

    /**
     * Delete stock item by given stockItemId.
     *
     * @param stockItemId
     */
    public void deleteByStockItemId(@NonNull final Integer stockItemId);
}
