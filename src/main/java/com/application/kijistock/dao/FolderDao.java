package com.application.kijistock.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.application.kijistock.entity.FolderEntity;

import lombok.NonNull;

/**
 * @author koki.esaki
 */
@Repository
public interface FolderDao extends JpaRepository<FolderEntity, String> {

    /**
     * Find folder by given userId.
     *
     * @param userId
     * @return list of StockItemEntity
     */
    public List<FolderEntity> findByUserId(@NonNull final String userId);

    /**
     * Delete folder by given folderId.
     *
     * @param folderId
     */
    public void deleteByFolderId(@NonNull final Integer folderId);
}
