package com.application.kijistock.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.application.kijistock.constant.KijiStockResponseResult;
import com.application.kijistock.entity.StockItemEntity;
import com.application.kijistock.exception.KijiStockException;
import com.application.kijistock.logic.StockItemLogic;
import com.application.kijistock.request.ItemStockRequest;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * @author koki.esaki
 */
@Slf4j
@Transactional
@Service
public class StockItemService {

    @Autowired
    StockItemLogic stockItemLogic;

    /**
     * Get stock item list by given userId and folderId.
     *
     * @param userId
     * @param folderId
     * @return list of StockItemEntity
     */
    public List<StockItemEntity> getStockItemList(@NonNull final String userId, @NonNull final String folderId) {
        final List<StockItemEntity> stockItemEntityList = stockItemLogic.findByUserIdAndFolderId(userId, folderId);
        if (CollectionUtils.isEmpty(stockItemEntityList)) {
            log.info("Data not found. userId: {}, folderId: {}", userId, folderId);
        }
        return stockItemEntityList;
    }

    /**
     * Stock item by given itemStockRequest.
     *
     * @param itemStockRequest
     * @throws KijiStockException
     */
    public void stockItem(@NonNull final ItemStockRequest itemStockRequest) throws KijiStockException {
        try {
            stockItemLogic.save(itemStockRequest);
        } catch (IllegalArgumentException | IOException | URISyntaxException e) {
            throw new KijiStockException(e.getMessage(), KijiStockResponseResult.CE999_UNEXPECTED_ERROR);
        }
    }

    /**
     * Delete stock item by given stockItemId.
     *
     * @param stockItemId
     */
    public void deleteStockItem(@NonNull final Integer stockItemId) {
        stockItemLogic.deleteByStockItemId(stockItemId);
    }
}