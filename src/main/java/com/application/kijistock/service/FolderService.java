package com.application.kijistock.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.application.kijistock.entity.FolderEntity;
import com.application.kijistock.logic.FolderLogic;
import com.application.kijistock.request.FolderSaveRequest;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * @author koki.esaki
 */
@Slf4j
@Transactional
@Service
public class FolderService {

    @Autowired
    FolderLogic folderLogic;

    /**
     * Get folder list by given userId.
     *
     * @param userId
     * @return list of FolderEntity
     */
    public List<FolderEntity> getFolderList(@NonNull final String userId) {
        final List<FolderEntity> folderEntityList = folderLogic.findByUserId(userId);
        if (CollectionUtils.isEmpty(folderEntityList)) {
            log.info("Data not found. userId: {}", userId);
        }
        return folderEntityList;
    }

    /**
     * Save folder by given folderSaveRequest.
     *
     * @param folderSaveRequest
     */
    public void saveFolder(@NonNull final FolderSaveRequest folderSaveRequest) {
        folderLogic.saveByUserIdAndFolderName(folderSaveRequest.getUserId(), folderSaveRequest.getFolderName());
    }

    /**
     * Delete folder by given folderId.
     *
     * @param folderId
     */
    public void deleteFolder(@NonNull final Integer folderId) {
        folderLogic.deleteByFolderId(folderId);
    }
}