package com.application.kijistock.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.application.kijistock.constant.KijiStockResponseResult;
import com.application.kijistock.exception.KijiStockException;
import com.application.kijistock.response.KijiStockResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class KijiStockExceptionHandler {

    @ExceptionHandler(value = { org.springframework.web.bind.MethodArgumentNotValidException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public KijiStockResponse<?> MethodArgumentNotValidException(final org.springframework.web.bind.MethodArgumentNotValidException e) {
        final KijiStockResponse<?> response = new KijiStockResponse<>();
        response.setResultCode(KijiStockResponseResult.CE002_VALIDATION_ERROR);
        response.setResultMessage("Request is invalid...");
        log.error("Request is invalid, message: {}", e.getMessage());
        return response;
    }

    @ExceptionHandler(value = { javax.validation.ConstraintViolationException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public KijiStockResponse<?> ConstraintViolationException(final javax.validation.ConstraintViolationException e) {
        final KijiStockResponse<?> response = new KijiStockResponse<>();
        response.setResultCode(KijiStockResponseResult.CE002_VALIDATION_ERROR);
        response.setResultMessage("Request is invalid...");
        log.error("Request is invalid, message: {}", e.getMessage());
        return response;
    }

    @ExceptionHandler(value = { KijiStockException.class })
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public KijiStockResponse<?> KijiStockException(final KijiStockException e) {
        final KijiStockResponse<?> response = new KijiStockResponse<>();
        response.setResultMessage(e.getResultMessage());
        response.setResultCode(e.getResultCode());
        log.error("KijiStockException occured, message: {}", e.getResultMessage());
        return response;
    }
}