package com.application.kijistock.exception;

import lombok.Getter;

public class KijiStockException extends RuntimeException {

    @Getter
    private String resultMessage;
    @Getter
    private String resultCode;

    public KijiStockException(final String resultMessage, final String resultCode) {
        super(resultMessage);
        this.resultMessage = resultMessage;
        this.resultCode = resultCode;
    }

    public KijiStockException(final String resultMessage, final String resultCode, final Throwable cause) {
        super(resultMessage, cause);
        this.resultMessage = resultMessage;
        this.resultCode = resultCode;
    }
}
