package com.application.kijistock.response;

import lombok.Data;

/**
 * @author koki.esaki
 */
@Data
public class KijiStockResponse<T> {

    /** Response Result Code. */
    private String resultCode;

    /** Response Result Message. */
    private String resultMessage;

    /** Response Data. */
    private T data;
}
