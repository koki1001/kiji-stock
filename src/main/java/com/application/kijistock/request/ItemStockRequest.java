package com.application.kijistock.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ItemStockRequest {

    /**  User ID. */
    @NotBlank
    private String userId;

    /**  Folder ID. */
    @NotBlank
    private String folderId;

    /**  URL. */
    @NotBlank
    private String url;
}
