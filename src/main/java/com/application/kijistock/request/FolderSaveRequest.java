package com.application.kijistock.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FolderSaveRequest {

    /** User ID. */
    @NotBlank
    private String userId;

    /** Folder Name. */
    @NotBlank
    private String folderName;
}
