DROP TABLE IF EXISTS stock_item CASCADE;
CREATE TABLE stock_item(
  stock_item_id serial PRIMARY KEY
 ,user_id text NOT NULL
 ,folder_id text NOT NULL
 ,title text NOT NULL
 ,url text NOT NULL
 ,content text NOT NULL
 ,favicon_path text NOT NULL
);

DROP SEQUENCE IF EXISTS stock_item_id_seq CASCADE;
CREATE SEQUENCE stock_item_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 10000
    START 1
    CACHE 1
    CYCLE;

DROP TABLE IF EXISTS folder CASCADE;
CREATE TABLE folder(
  folder_id serial PRIMARY KEY
 ,user_id text NOT NULL
 ,name text NOT NULL
);

DROP SEQUENCE IF EXISTS folder_id_seq CASCADE;
CREATE SEQUENCE folder_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 10000
    START 1
    CACHE 1
    CYCLE;